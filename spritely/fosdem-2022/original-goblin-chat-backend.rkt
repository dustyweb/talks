#lang racket

(provide ^chatroom
         spawn-user-controller-pair)

(require goblins
         goblins/actor-lib/methods
         goblins/actor-lib/actor-sealers
         goblins/actor-lib/common
         goblins/actor-lib/ward
         goblins/actor-lib/pubsub)

;;; =============
;;; Backend stuff
;;; =============

(define (call-with-unsealed-user-message from-user sealed-msg
                                         proc)
  ;; Unseal from the user
  (define user-chat-unsealer-vow
    (<- from-user 'get-chat-unsealer))
  (define unsealed-msg-vow
    (<- user-chat-unsealer-vow sealed-msg))
  (on unsealed-msg-vow proc))

(define (^chatroom _bcom name)
  (define-spawned subscribers
    ^hasheq)

  (define send-to-subscribers
    (make-keyword-procedure
     (lambda (kws kw-vals . args)
       (for ([subscriber ($ subscribers 'values)])
         (keyword-apply <-np kws kw-vals subscriber args)))))

  (define (^user-messaging-channel bcom associated-user user-inbox)
    (define main-beh
      (methods
       [(leave)
        ($ subscribers 'remove associated-user)
        (send-to-subscribers 'user-left associated-user)
        (bcom dead-beh)]
       ;; In this version we don't even bother checking if the user
       ;; was the one that sent the message or not... instead we structure
       ;; things such that the message will error out when recieved by
       ;; clients.  However it is nicer to check the predicate probably.
       [(send-message sealed-message)
        (send-to-subscribers 'new-message associated-user sealed-message)
        'OK]
       ;; This state should be propagated (but can be occasionally stale)
       ;; so I don't know that we really need to have this method...
       [(list-users)
        ($ subscribers 'keys)]))
    (define dead-beh
      (lambda _ 'CONNECTION-CLOSED))
    main-beh)

  (define (^finalize-subscription bcom associated-user)
    (define (pre-finalize-beh user-inbox)
      ;; send to subscribers other than this user first
      (send-to-subscribers 'user-joined associated-user)
      ;; now subscribe this user
      ($ subscribers 'set associated-user user-inbox)
      (for [(present-user ($ subscribers 'keys))]
        (<-np user-inbox 'user-joined present-user))
      (bcom post-finalize-beh
            ;; We need to give both the user messaging channel as a way
            ;; for the user to communicate with us but also we should give
            ;; the current list of users, since that might change later
            (spawn ^user-messaging-channel associated-user user-inbox)))
    (define (post-finalize-beh . _args)
      (error "Already finalized!"))
    pre-finalize-beh)

  (methods
   [(self-proposed-name) name]
   ;; Return a sealed user messaging channel, sealed for that user
   ;; specifically.  If they can open it they can complete the
   ;; subscription process.
   [(subscribe user)
    (define subscription-sealer-vow
      (<- user 'get-subscription-sealer))
    ;; TODO: throw an error if the user is already in the chat
    (<- subscription-sealer-vow
        (spawn ^finalize-subscription user))]))

;;; Here we spawn a user and also an actor that can control that user
;;; Return both as a cons cell of (user . user-controller)
(define (spawn-user-controller-pair self-proposed-name)
  (define-values (chat-msg-sealer chat-msg-unsealer chat-msg-sealed?)
    (spawn-sealer-triplet))
  (define-values (subscription-sealer subscription-unsealer subscription-sealed?)
    (spawn-sealer-triplet))
  (define-values (controller-warden controller-incanter)
    (spawn-warding-pair))

  (define-spawned rooms->inboxes/channels
    ^hasheq)
  (define-spawned client-subscribers
    ^seteq)
  
  ;; Here's our user object.  More or less it's a profile that provides
  ;; a self-proposed name, an unsealer, and a predicate to check whether
  ;; we sealed things
  (define (^user _bcom)
    (methods
     [(self-proposed-name)
      self-proposed-name]
     [(get-chat-sealed?)
      chat-msg-sealed?]
     [(get-chat-unsealer)
      chat-msg-unsealer]
     [(get-subscription-sealer)
      subscription-sealer]))
  (define user
    (spawn ^user))

  (define send-to-clients
    (make-keyword-procedure
     (lambda (kws kw-vals . args)
       (for ([client ($ client-subscribers 'data)])
         (keyword-apply <-np kws kw-vals client args)))))

  (define (^user-inbox bcom context)
    (define room-users
      (spawn ^seteq))
    (define inbox-pubsub
      (spawn ^pubsub))

    (define controller-methods
      (methods
       [(revoke)
        (bcom revoked-beh)]
       [(subscribe subscriber)
        ($ inbox-pubsub 'subscribe subscriber)
        (for ([user ($ room-users 'data)])
          (<-np subscriber 'user-joined user))]))
    (define public-methods
      (methods
       [(new-message from-user sealed-msg)
        ;; The purpose of calling call-with-unsealed-user-message is to
        ;; ensure that it unseals correctly
        (call-with-unsealed-user-message
         from-user sealed-msg
         (lambda (unwrapped-user-msg)
           ($ inbox-pubsub 'publish
              'new-message context from-user unwrapped-user-msg)))]
       [(user-joined user)
        ($ room-users 'add user)
        ($ inbox-pubsub 'publish
           'user-joined user)]
       [(user-left user)
        ($ room-users 'remove user)
        ($ inbox-pubsub 'publish
           'user-left context user)]
       [(context) context]))

    ;; Now for the two primary states.
    ;; Either we are in an "enabled" state or a "revoked" state.

    ;; When enabled, main access is enabled
    (define enabled-beh
      (ward controller-warden controller-methods
            #:extends public-methods))

    ;; but when revoked, only admin access is enabled
    (define revoked-beh
      (make-keyword-procedure
       (lambda _
         (error "Revoked!"))))

    enabled-beh)

  ;; Basically a channel which is set up with our sealers.
  (define (^authenticated-channel _bcom room-channel inbox)
    (methods
     [(send-message contents)
      (<- room-channel 'send-message ($ chat-msg-sealer contents))]
     [(subscribe subscriber)
      (define ((^unsubscribe bcom))
        ($ controller-incanter inbox 'unsubscribe subscriber))
      ($ controller-incanter inbox 'subscribe subscriber)
      ;; give the user a way to unsubscribe
      `#(OK ,(spawn ^unsubscribe))]
     #:extends room-channel))

  ;; TODO: resume here with all the race condition stuff
  (define (^user-controller _bcom)
    (methods
     [(whoami) user]
     ;; This creates weird race conditions I think... we need an unum
     ;; kind of thing in order for it to work ok.
     [(connect-client subscriber)
      ($ client-subscribers 'add subscriber)
      `#(OK ,($ rooms->inboxes/channels 'keys))]
     [(join-room room)
      (when ($ rooms->inboxes/channels 'has-key? room)
        (error "Already subscribed to room"))

      ;; Make a new inbox specifically for this purpose, and associate
      ;; it with this room
      (define inbox
        (spawn ^user-inbox room))
      (define sealed-finalizer-vow
        (<- room 'subscribe user))
      ;; First we request to subscribe, unseal it
      (define subscription-finalizer-vow
        (on sealed-finalizer-vow
            (lambda (sealed-finalizer)
              ($ subscription-unsealer sealed-finalizer))
            #:promise? #t))
      ;; If all goes well, we can finalize (letting the other side set up)
      ;; and get the relevant inbox.
      (on (<- subscription-finalizer-vow inbox)
          (lambda (room-channel)
            (define authenticated-channel
              (spawn ^authenticated-channel room-channel inbox))
            ($ rooms->inboxes/channels 'set room authenticated-channel)
            ;; TODO: this should actually be the subscription
            (send-to-clients 'we-joined-room room authenticated-channel)
            authenticated-channel)
          #:promise? #t)]))
  (define user-controller
    (spawn ^user-controller))
  (cons user user-controller))

