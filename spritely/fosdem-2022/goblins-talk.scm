(define-module (goblins-talk)
  #:use-module (goblins core)
  #:use-module (goblins vrun)
  #:use-module (goblins actor-lib methods))

(define (^greeter _bcom our-name)
  (lambda (your-name)
    (format #f "Hello ~a, my name is ~a!"
            your-name our-name)))

(define* (^cell bcom #:optional [val #f])
  (case-lambda
    (()         ; get
     val)
    ((new-val)  ; set
     (bcom (^cell bcom new-val)))))

(define* (^mcell bcom #:optional [val #f])
  (methods
   ((get)
    val)
   ((set new-val)
    (bcom (^mcell bcom new-val)))))

(define (^cgreeter _bcom our-name)
  (define times-called
    (spawn ^mcell 0))
  (methods
   ((get-times-called)
    ($ times-called 'get))
   ((greet your-name)
    (pk 'before-incr ($ times-called 'get))
    ;; increase the number of times called
    ($ times-called 'set
       (+ 1 ($ times-called 'get)))
    (pk 'after-incr ($ times-called 'get))
    (error "Yikes")
    (format #f "[~a] Hello ~a, my name is ~a!"
            ($ times-called 'get)
            your-name our-name))))

(define (^long-distance-caller _bcom my-name)
  (lambda (call-me)
    (on (<- call-me 'greet my-name)
        (lambda (got-back)
          (format #t "<~a> Heard back: ~a\n"
                  my-name got-back)))))

