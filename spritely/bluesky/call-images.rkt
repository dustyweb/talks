#lang racket

(provide search-interface incoming-call-mom incoming-call-edge-names)

(require slideshow)

#;(define (filled-frame pct
                      #:width [width (pict-width pct)]
                      #:height [height (pict-height pct)]
                      #:color [color "white"]
                      #:draw-border? [draw-border? #t]
                      #:border-color [border-color #f]
                      #:border-width [border-width #f])
  (ct-superimpose
   (filled-rectangle width height
                     #:color color
                     #:draw-border? draw-border?
                     #:border-width border-width
                     #:border-color border-color)
   pct))

(define search-interface
  (scale
   (cc-superimpose
    (filled-rounded-rectangle 185 225 5
                              #:color "white"
                              #:draw-border? #f)
    (vl-append
     10
     (hc-append 10
                (text "Search: " (cons 'bold 'default))
                (let ([name-text
                       (inset
                        (text "ben")
                        5)])
                  (lc-superimpose
                   (filled-rounded-rectangle
                    100 (pict-height name-text)
                    5
                    #:color "white")
                   name-text)))
     (blank 0 10)
     (text "Personal contacts:" (cons 'bold 'default))
     (text "  ☎ Ben Grossmeier")
     (text "  ☎ Benjamin Gonwick")
     
     (blank 0 5)
     (text "Network contacts:" (cons 'bold 'default))
     (text "  ☎ Alyssa ➞ Ben Bitdiddle")))
   2))


(define (text-button str #:width [width #f])
  (let* ([name-text
          (inset (text str) 15 5)]
         [width (or width (pict-width name-text))]
         [height (pict-height name-text)])
    (cc-superimpose
     (inset
      (filled-rounded-rectangle width height 5
                                #:color "gray"
                                #:draw-border? #f)
      2 3 0 0)
     (filled-rounded-rectangle width height 5
                               #:color "white")
     name-text)))

(define incoming-call-mom
  (scale
   (cc-superimpose
    (filled-rounded-rectangle 185 225 5
                              #:color "white"
                              #:draw-border? #f)
    (vc-append
     (text "☎" null 25)
     (text "Mom" (cons 'bold 'default) 14)
     (blank 0 10)
     (hc-append 5
                (text-button "accept" #:width 70)
                (text-button "decline" #:width 70))))
   2))

(define incoming-call-edge-names
  (scale
   (cc-superimpose
    (filled-rounded-rectangle 185 225 5
                              #:color "white"
                              #:draw-border? #f)
    (vc-append
     (text "☎" null 25)
     (text "Alyssa ➞ Jane Nym" (cons 'bold 'default) 12)
     ;; TODO: Remove this?
     (blank 0 2)
     (text "Faculty ➞ Dr. Nym" (cons 'bold 'default) 10)
     (blank 0 10)
     (hc-append 5
                (text-button "accept" #:width 70)
                (text-button "decline" #:width 70))))
   2))
