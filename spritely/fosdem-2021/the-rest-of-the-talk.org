 - Start out with "main" presentation https://dustycloud.org/misc/spritely-fosdem.pdf
 - Tired cat https://commons.wikimedia.org/wiki/File:Tired_20-year-old_cat.jpg#/media/File:Tired_20-year-old_cat.jpg
 - CapTP drawing https://dustycloud.org/misc/3vat-handoff-scaled.jpg
 - Everything bagel https://en.wikipedia.org/wiki/Everything_bagel#/media/File:20090424_Bagels_001.JPG
 - 8sync / mudsync https://www.gnu.org/software/8sync/
 - guiler's year of racket https://ftp.heanet.ie/mirrors/fosdem-video/2019/K.4.201/guileracket.webm 9:25
 - Spritely website https://spritelyproject.org/
 - Goblins docs https://docs.racket-lang.org/goblins/index.html

 - Time travel blogpost https://dustycloud.org/blog/goblins-time-travel-micropreview/
 - Communication/goblin chat blogpost https://dustycloud.org/blog/spritely-goblins-v0.7-released/

 - Back to the presentation https://dustycloud.org/misc/spritely-fosdem.pdf

 - 4th wall breakage again...

 - CapTP drawing https://dustycloud.org/misc/3vat-handoff-scaled.jpg
 - Everything bagel https://en.wikipedia.org/wiki/Everything_bagel#/media/File:20090424_Bagels_001.JPG

 - Racket website https://racket-lang.org/
 - Guile website https://www.gnu.org/software/guile/
 - Goblins page https://spritelyproject.org/#goblins
 - CapTP drawing https://dustycloud.org/misc/3vat-handoff-scaled.jpg
 - Agoric's website https://agoric.com/
 - Agoric's CapTP https://github.com/Agoric/agoric-sdk/issues/1827
 - Mes https://www.gnu.org/software/mes/manual/html_node/The-Mes-Bootstrap-Process.html
 - Javascript Dystopian novella https://dustycloud.org/blog/javascript-packaging-dystopia/
 - WASM https://webassembly.org/
 - pola would have prevented the event stream incident https://medium.com/agoric/pola-would-have-prevented-the-event-stream-incident-45653ecbda99
 - backdoor injected into the linux kernel https://lwn.net/Articles/57135/
 - lambda papers https://en.wikisource.org/wiki/Lambda_Papers
 - Security kernel http://mumble.net/~jar/pubs/secureos/secureos.html
 - Solitaire https://en.wikipedia.org/wiki/Solitaire#/media/File:KPatience.png
 - Guile sandboxing https://www.gnu.org/software/guile/manual/html_node/Sandboxed-Evaluation.html
 - Genode https://genode.org/
 - sandstorm? https://sandstorm.io/
 - Cap'n Proto https://capnproto.org/
 - Guix https://guix.gnu.org/
 - Shepherd https://www.gnu.org/software/shepherd/
 - Hurd https://guix.gnu.org/en/blog/2020/childhurds-and-substitutes/
 - SeL4 https://sel4.systems/
 - Polaris https://www.hpl.hp.com/techreports/2004/HPL-2004-221.pdf
 - Plash http://www.cs.jhu.edu/~seaborn/plash/html/
 - bandsocks https://github.com/scanlime/bandsocks
 - Ludo's writeup on the pola wrapper https://lists.gnu.org/archive/html/help-guix/2020-12/msg00134.html
 - Hygiene for a computing pandemic https://fossandcrafts.org/episodes/20-hygiene-for-a-computing-pandemic.html
 - Ending?
   https://commons.wikimedia.org/wiki/File:Good_Morning_From_the_International_Space_Station.jpg#/media/File:Good_Morning_From_the_International_Space_Station.jpg


