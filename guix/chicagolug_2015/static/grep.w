define-public grep
  package
    name "grep"
    version "2.21"
    source
      origin
        method url-fetch
        uri : string-append "mirror://gnu/grep/grep-"
                            . version ".tar.xz"
        sha256
          base32
            . "1pp5n15qwxrw1pibwjhhgsibyv5cafhamf8lwzjygs6y00fa2i2j"
        patches : list : search-patch "grep-CVE-2015-1345.patch"
    build-system gnu-build-system
    synopsis "Print lines matching a pattern"
    description
      . "grep is a tool for finding text inside files.  Text is found by
matcing a pattern provided by the user in one or many files.  The pattern
may e provided as a basic or extended regular expression, or as fixed
strigs.  By default, the matching text is simply printed to the screen,
howeer the output can be greatly customized to include, for example, line
numbrs.  GNU grep offers many extensions over the standard utility,
inclding, for example, recursive directory searching."
    license gpl3+
    home-page "http://www.gnu.org/software/grep/"
