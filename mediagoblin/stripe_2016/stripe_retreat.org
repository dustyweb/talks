#+TITLE: MediaGoblin and the Stripe Retreat
#+AUTHOR:    Christopher Allan Webber
#+EMAIL:     cwebber@creativecommons.org
#+DATE:      2016-03-29
#+LANGUAGE:  en
#+OPTIONS:   H:2 num:t toc:nil \n:nil @:t ::t |:t ^:nil -:nil f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: Frankfurt
#+BEAMER_COLOR_THEME: dolphin
#+BEAMER_FRAME_LEVEL: 2

#+BEGIN_LATEX
\newcommand{\wideimage}[1] {
  \begin{center}
    \includegraphics[width=\textwidth]{#1}
  \end{center}
}
#+END_LATEX

#+BEGIN_LATEX
\newcommand{\heightimage}[2] {
  \begin{center}
  \includegraphics[height=#2]{#1}
  \end{center}
}
#+END_LATEX

* Setting the stage
** Who am I?

  \heightimage{../../network_freedom/static/libreplanet_toon_chris.png}{5cm}

** The web we want

  \wideimage{../../guix/static/gmg_campaign_healthy_internet.png}

** The sad reality (centralization)

  \wideimage{../../guix/static/gmg_campaign_fragile_internet.png}

** The sad reality (censorship)

  \wideimage{../static/censor_scan.png}

** The sad reality (surveillance)

  \wideimage{../../guix/static/gmg_campaign_infected_node.png}

** The sad reality (fragility)

  \wideimage{../../guix/static/gmg_campaign_fragile_internet.png}

** The sad reality (fragility)

  \wideimage{../../guix/static/gmg_campaign_fragile_internet_breaking.png}

** The sad reality (fragility)

  \wideimage{../../guix/static/gmg_campaign_fragile_internet_broken.png}

** The sad reality (fragility)

  \wideimage{../../guix/static/gmg_campaign_fragile_internet_gone.png}

** So I work on MediaGoblin...

  \heightimage{../static/mediagoblin_mascot.png}{7cm}

** Decentralized media publishing

  \wideimage{../../guix/static/caminandes-on-mediagoblin.png}

** Goals for the retreat?

 - Get MediaGoblin to 1.0
 - Advance federation in MediaGoblin
 - Advance federation standards
 - Improve deployability

* What we did
** 0.9.0: The Three Goblineers release!  (Today!)

  \wideimage{../static/three_goblineers-crop.png}

** About databases (refactoring, migrations, tombstones)

# TODO: Fix this slide

:               *                      *  
:  *                      *                  _.     *
:       *           *          *            <  '.
:              *                             )  )       *
:                                     *     <_.'   *
:     *      *        .-------------.
:                   .'               '.                *
:        *          |                 |   *
:                   |   TOMB OF THE   |       *
:             *     |     UNKNOWN     |            *
:    *              | ACTIVITYSTREAMS |
:                   |     OBJECT      |
:              .^.  |                 |
:   _  .+.  _  |~|  |    ????-????    |  .+. .-.  _  .-.
:  | | |~| |=| | |  |                 |  |=| |~| | | |"|
: ``'`'`''``'`'`'`'``'``'`'`''``'`'`'`'``'`''``''``'`'`''

** About deployment...

  \wideimage{../../guix/static/upgrade_softwares.png}

** Deployment and Guix

  \heightimage{../../guix/static/guixsd_logo.png}{6.75cm}

** What about federation?

# TODO: Switch to people exchanging documents

  \wideimage{../../network_freedom/static/purple_federation.png}

** Federation: We know what we want

  \wideimage{../../federation_python/static/federation-unsynced.png}

** Federation: We know what we want

  \wideimage{../../federation_python/static/federation-synced.png}

** Federation... it's coming!  (Thank you, Jessica Tallon!)

  \heightimage{../static/jessica_demos_federation.png}{6.75cm}

** Standards!  ActivityPub and etc.

  \wideimage{../static/2015-03-18-w3c-f2f-crop.jpg}

** Standards progress!  (FPWD wooooo!)

  \wideimage{../static/activitypub_fpwd.png}

** Conference: FOSDEM!

  \heightimage{../../guix/chicagolug_2015/static/new_guile_website-crop.png}{5cm}

Collaborating with Guile/Guix folks

How can we improve communities?

** Conference: LibrePlanet!

  \heightimage{../static/protesting_eme_before_microsoft.png}{6cm}

Sussman, W3C Face to Face, Spinachcon, Guix talk, DRM protest...

** Bonus: Emacs hackathon! Guile emacs!

  \wideimage{../static/emacs_hackathon.jpg}

** Bonus: Collaborating with locals!

  \heightimage{../static/nonfree/sandstorm_cat.png}{5cm}

Sandstorm, Stellar...

* Wrapping up
** How'd we do?

 - *Federation:* On track
 - *1.0:* Not out yet, but on track
 - *Deployment:* Lots of progress
 - And lots of little things!
 - Seems pretty good!

** Thoughts about the retreat

 - The retreat is great!
 - Very self directed...
 - Stripe is really welcoming
 - Housing is tricky
 - No matter how much you do, you'll wish you did more
 - But look how much farther along we are!

** Please continue the retreat!

  \wideimage{../static/less_than_three.png}

** COMMENT Credits

# three goblineers: Myself and Morgan
# w3c face to face meeting picture by aaronpk
# sandstorm cat by definitely not us
# FSF eme protest image
# Guile image

** Thanks!

  \heightimage{../../guix/static/mediagoblin_mesh.png}{4cm}

  © 2016 Christopher Allan Webber =<cwebber@dustycloud.org>=

  This presentation is licensed under the
  [[https://creativecommons.org/licenses/by-sa/4.0/][Creative Commons Attribution Share-Alike 4.0 International]]
  license.

  More on MediaGoblin: http://mediagoblin.org/

  More on Guix: https://gnu.org/software/guix
