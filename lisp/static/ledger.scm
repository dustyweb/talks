(use-modules (rx irregex))

(define example-entry
  "2004/09/29  My Employer
    Assets:Checking               $500.00
    Income:Salary")

(define date-syntax
  '(: (? (=> year (= 4 num)) "/")
      (=> month (? num) num) "/"
      (=> day (? num) num)))

(define header-line
  `(: ,date-syntax
      (+ space)
      (? (=> cleared #\*) (+ space))
      (=> payee alphanumeric (*? (or alphanumeric space)))
      (* space)))

(define currency-or-commodity
  '(+ (~ #\. #\, #\/ #\@ (/ "09") space)))

(define numeric-amount
  '(: (? #\-)
      (or
       (+ (or num #\,))                         ; just numbers and commas
       (: (* (or num #\,)) #\. (+ num))         ; floating point with head
       (: (+ (or num #\,)) #\. (* num)))))      ; floating point with tail

(define monetary-amount
  ;; @@: There seem to be some more specific rules we're missing...
  ;;   ledger seems to know about *some* currencies as EUR, DM, etc
  `(or
    ;; before implies probably currency (ledger logic!)
    (: (=> currency ,currency-or-commodity)
       (* space)
       (=> amount ,numeric-amount))
    ;; after means commodity (ledger logic!)
    (: (=> amount ,numeric-amount)
       (* space)
       (=> commodity ,currency-or-commodity))))

(define account-atom
  '(: (+ (or (- alphanumeric #\:) space))
      (- alphanumeric ": ")))

(define account-syntax
  `(: ,account-atom
      (* #\: ,account-atom)))

(define account-line-syntax
  `(: (+ space)
      (=> account ,account-syntax)
      (* space)
      (? (=> amount ,monetary-amount))
      (* space)))

(define ledger-entry-syntax
  `(: bol
      ,header-line
      eol
      (+ ,account-line-syntax eol)))

(define ledger-entry-rx
  (irregex ledger-entry-syntax))
